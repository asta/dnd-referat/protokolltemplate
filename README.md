# AStA Uni Göttingen Pandoc LaTeX Template

*2020-06-30*

Welcome to the AStA Uni Göttingen Pandoc LaTeX Template. This is an early version of the template and this README file is not complete. Additional content and clarifications will be added in the comming weeks.

## Purpose

The AStA-Uni-Goettingen template for the document conversion software *Pandoc* can be used to turn *Markdown* documents into PDF files. When creating a PDF file, the document title and other attributes may be provided as meta data. By default the output file contains a colorful title page and a colored background throughout the document. However, a version with reduced color can be created to allow for printing-friendly documents.

## Parameters

For the conversion of a document you can specify any of the following parameters, if you want to assign a value different from the default. Additionally, you can use any of Pandocs built in parameters.

```yaml
---
reduce-color: true                # default: false
hide-links: true                  # default: false
titlepage: true                   # default: false
document-type: "Bericht"          # default: "Protokoll"
institution: "Beispiel Referat"   # default: empty
event-type: "Plenum"              # default: empty
date: "24.06.2020"                # default: empty
lang: "de"                        # default: "en"
title: "Beispiel-Bericht"         # for the header, default: empty
...
```

The ```title``` parameter will be ignored in future updates of this Template.

## Requirements

To use the AStA-Uni-Goettingen template to convert a *Markdown* document into a PDF file, you need have the document conversion software *Pandoc* and *LaTeX* installed. Additionally you need the actual template file

- ```AStA-Uni-Goettingen-V02.latex```

and the page-background files

- ```AStA-Protokolle-Background-Cover-V02.pdf```
- ```AStA-Protokolle-Background-Inner-V02.pdf```
- ```AStA-Protokolle-Background-Cover-Light-V04.pdf```
- ```AStA-Protokolle-Background-Inner-Light-V01.pdf```

## Usage

To convert a *Markdown* document into a PDF file, follow these steps

1. Create the templates folder ```<home>/.pandoc/templates/```, if it does not exist.

2. If the templates folder does not contain the template file ```AStA-Uni-Goettingen-V02.latex```, place the latter in the former.

3. Navigate to the document directory that contains the *Markdown* file you want to convert.

4. If the document directory does not contain the page-background files listed under *Requirements*, place these in the former.

5. Specify the additional parameters and perform the conversion by following one of these three strategies:

   1. Add a *YAML* metadata block at the top of the *Markdown* file to be converted, containing values for the relevant parameters. The metadata block should start with a line containing only ```---``` and end with a line containing only ```...```.
      
      ```yaml
      ---
reduce-color: true                
hide-links: false                  
titlepage: true                   
document-type: "Bericht"          
institution: "Beispiel Referat"   
event-type: "Plenum"              
date: "24.06.2020"                
lang: "de"                        
...

      Document content
      ```
      
      In the command line, run the following command to create a PDF file:
      
```bash
      pandoc Input.md -o Output.pdf --from markdown --template AStA-Uni-Goettingen-V02
```
      
Here ```Input``` and ```Output``` need to be replaced with the name of the input file and the desired name of the output file.
      
   2. TODO: Use a separate YAML file
   
   3. TODO: Specify parameters in the command line
